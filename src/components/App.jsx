import React, { useState } from 'react';
import ResourceList from './ResourceList'
import UserResourcesList from './UserResourcesList';

const App = () =>{
  const [ resource, setResource ] = useState("posts");
  
  return(
    <div>
      <UserResourcesList/>
      <div>
        <button onClick={()=> setResource('posts')}>posts</button>
        <button onClick={()=> setResource('todos')}>todos</button>
      </div>
      <ResourceList resource={resource}/>
    </div>
  )
}

export default App;